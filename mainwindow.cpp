#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->Calculate,SIGNAL(clicked()),this,SLOT(update_overall()));

}
MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::update_overall()
{
    compute_overall();
    double score = static_cast<double>(finalscore);
    ui->grade->setText(QString::number(score));
}

void MainWindow::compute_overall()
{
//find lowest hw
   double lowesthw = ui->hw1score->value();
   if(lowesthw>=ui->hw2score->value())
       lowesthw = ui->hw2score->value();
   if(lowesthw>=ui->hw3score->value())
       lowesthw = ui->hw3score->value();
   if(lowesthw>=ui->hw4score->value())
       lowesthw = ui->hw4score->value();
   if(lowesthw>=ui->hw5score->value())
       lowesthw = ui->hw5score->value();
   if(lowesthw>=ui->hw6score->value())
       lowesthw = ui->hw6score->value();
   if(lowesthw>=ui->hw7score->value())
       lowesthw = ui->hw7score->value();
   if(lowesthw>=ui->hw8score->value())
       lowesthw = ui->hw8score->value();
   //set sum of all hw scores minus the lowest
   double hw = ui->hw1score->value()+ui->hw2score->value()+ui->hw3score->value()+ui->hw4score->value()+ui->hw5score->value()+ui->hw6score->value()+ui->hw7score->value()+ui->hw8score->value()-lowesthw;
   double asd = static_cast<double>(hw);
   //find higest midter
   double highestmidterm;
   if (ui->mid1_2->value()>=ui->mid2_2->value())
       highestmidterm = ui->mid1_2->value();
   else
       highestmidterm = ui->mid2_2->value();
   //schemes
   double a = hw/7*.25+.2*ui->mid1_2->value()+.2*ui->mid2_2->value()+.35*ui->final_3->value();
   double b = hw/7*.25+.3*highestmidterm+.44*ui->final_3->value();
   //grading schemes
   if(ui->op1->isChecked())
     finalscore = a;
   else if (ui->op2->isChecked())
       finalscore = b;
   else//find best grading scheme
   {
       if (a>b)
       {
          finalscore =  a;
          ui->op1->click();
       }
       else
       {
           finalscore =b;
           ui->op2->click();
       }
   }

}
