#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void compute_overall();
    void update_overall();
private:
    Ui::MainWindow *ui;
    double finalscore;
};

#endif // MAINWINDOW_H
